define([
	'require',
	'madjoh_modules/page_manager/tools',
	'settings/images/images',
],
function(require, Tools, Images){
	var tools = new Tools();
	
	var LoadingAnimation = {
		init : function(){
			var promises = [];

			for(var key in Images){
				promises.push(LoadingAnimation.setupImage(key));
			}
			return Promise.all(promises);
		},

		setupImage : function(imageKey){
			var image = document.createElement('img');
				image.promise = {};
				image.promise.promise = new Promise(function(resolve, reject){
					image.promise.resolve 	= resolve;
					image.promise.reject 	= reject;
				});
				image.onload = function(){
					image.promise.resolve();
				}
				image.onerror = function(){
					console.log('Error loading image', image.src);
					image.promise.resolve();
				}
			tools.setSource(image, imageKey);
			return image.promise.promise;
		}
	};

	return LoadingAnimation;
});