# loading_animation #

v1.1.1

** This documentation is out dated **

This module handles the loading animation view. 
It shows how images are preloaded and how ressources like the page and facebook are loaded.

## Gettings Started ##

For this code to work you need to set up some settings.

### Game Settings ###

First you need to place the progress bar somewhere in your html

```html
<progress id="loadingProgress"></progress>
```

In this JavaScript file, you need to specify if facebook is enabled or not :

```js
var dependencies = {};
var list = MadJohRequire.getList(dependencies);
define(list, function(require){
	var GameSettings = {
		version : '1.0.0',
		name : 'AppName',

		facebook : true		//!\\ Important line //!\\
	};

	return GameSettings;
});
```

### LoadingSettings ###

There you control the callback when the loading is incremented and when it's done :

```js
var dependencies = {
	CustomEvents : '1.0.0'
};
var list = MadJohRequire.getList(dependencies);
define(list, function(require, FlashTransition, CustomEvents){
	var LoadingSettings = {
		onPageLoaded : function(){
			if(document.profile_sync){
				CustomEvents.fireCustomEvent(document, 'LoggedIn');
			}else if(!document.preferences){
				FlashTransition.flashParam(1,0);
			}
		},
		onIncrement : function(nb, total){
			console.log(nb+'/'+total);
		}
	};

	return LoadingSettings;
});
```

### PreloadedImages ###

In this file you need to define all the sources to preload.

** Example **

```js
var dependencies = {
	'Styling' : '1.0.0',
	'Gallery' : '1.0.0'
};
var list = MadJohRequire.getList(dependencies);
define(list, function(require, Styling, Gallery){
	var PreloadedImages = [];

	// GENERIQUES GLOBAL
		PreloadedImages.push('MadJohModules/Templates/Gallery/'+Styling.deviceSize+'/btn_facebook.png');
		PreloadedImages.push('MadJohModules/Templates/Gallery/'+Styling.deviceSize+'/asset_facebook_small.png');

		PreloadedImages.push('MadJohModules/Templates/Gallery/'+Styling.deviceSize+'/btn_madjoh.png');
		PreloadedImages.push('MadJohModules/Templates/Gallery/'+Styling.deviceSize+'/asset_madjoh_small.png');
		PreloadedImages.push('MadJohModules/Templates/Gallery/'+Styling.deviceSize+'/asset_madjoh_create_small.png');
		PreloadedImages.push('MadJohModules/Templates/Gallery/'+Styling.deviceSize+'/asset_offline_small.png');

		PreloadedImages.push('MadJohModules/Templates/Gallery/'+Styling.deviceSize+'/ajax_loading.gif');

	// GENERIQUES LOCAL
		PreloadedImages.push('app/images/'+Styling.deviceSize+'/Backgrounds/homeBackground.png');
		PreloadedImages.push('app/images/'+Styling.deviceSize+'/Backgrounds/titleImg.png');
		PreloadedImages.push('app/images/'+Styling.deviceSize+'/Backgrounds/title_mini.png');

	return PreloadedImages;
});
```